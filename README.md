# Integration Testing with Flask

## Prerequisites

This tutorial should be completed on a VM running Ubuntu 18.06 or later, using Python 3.6.9 or above.

Download Chromium Browser and the chromedriver with the following commands:

```bash
cd ~
sudo apt-get update
sudo apt-get install -y unzip
sudo apt-get install -y chromium-browser
wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
```

The chromedriver will be unzipped onto your user's root directory (`~`).

Clone down this repository, create and activate your Python virtual environment and install pip dependencies:

```bash
sudo apt update && sudo apt install -y python3-venv python3-pip
git clone https://gitlab.com/qacdevops/todo-list-integration-testing.git
cd todo-list-integration-testing
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```